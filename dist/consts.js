"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CATEGORY_TO_PROPERTIES = exports.LATITUDE_LINE = exports.LATITUDE = void 0;
exports.LATITUDE = 31.43774097567021;
exports.LATITUDE_LINE = {
    type: 'Feature',
    properties: {},
    geometry: {
        coordinates: [
            [
                34.34252306591986,
                exports.LATITUDE
            ],
            [
                34.45003171797467,
                exports.LATITUDE
            ]
        ],
        type: 'LineString'
    }
};
exports.CATEGORY_TO_PROPERTIES = {
    hospital: { symbol: 'hospital', color: '#bd0000' },
    school: { symbol: 'college', color: '#8500cc' },
    mosque: { symbol: 'religious-muslim', color: '#000000' },
    government: { symbol: 'castle', color: '#008f00' },
    arsenal: { symbol: 'fire-station', color: '#cc7700' },
    'rockets-site': { symbol: 'rocket', color: '#0054ad' },
};
//# sourceMappingURL=consts.js.map