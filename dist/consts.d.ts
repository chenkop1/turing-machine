import { GeoJsonFeature } from './types';
export declare const LATITUDE = 31.43774097567021;
export declare const LATITUDE_LINE: GeoJsonFeature;
export declare const CATEGORY_TO_PROPERTIES: Record<string, {
    symbol: string;
    color: string;
}>;
//# sourceMappingURL=consts.d.ts.map