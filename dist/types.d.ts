export type Category = string;
type Longitude = number;
type Latitude = number;
export interface Target {
    readonly category: Category;
    readonly longitude: Longitude;
    readonly latitude: Latitude;
}
export interface TargetsAmountPartition {
    above: number;
    below: number;
}
export interface DecodeFunc {
    (text: string): string;
}
export interface GeoJson {
    readonly type: 'FeatureCollection';
    readonly features: GeoJsonFeature[];
}
export interface GeoJsonProperties {
    readonly 'category': Category;
    readonly 'marker-color': string;
    readonly 'marker-size': 'small' | 'medium' | 'large';
    readonly 'marker-symbol': string;
}
type Coordinate = [longitude: Longitude, latitude: Latitude];
interface GeoJsonPointGeometry {
    readonly coordinates: Coordinate;
    readonly type: 'Point';
}
interface GeoJsonLineGeometry {
    readonly coordinates: Coordinate[];
    readonly type: 'LineString';
}
export interface GeoJsonFeature {
    readonly type: 'Feature';
    readonly properties: Partial<GeoJsonProperties>;
    readonly geometry: GeoJsonPointGeometry | GeoJsonLineGeometry;
    readonly id?: number;
}
export {};
//# sourceMappingURL=types.d.ts.map