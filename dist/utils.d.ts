import { GeoJson, Target } from "./types";
export declare const reduce: <T>(reduceFunc: (acc: T, val: T) => T) => (arr: T[]) => T;
export declare const specialDecode: (shift: number) => (str: string) => string;
export declare const toGeoJson: (targets: Target[]) => GeoJson;
//# sourceMappingURL=utils.d.ts.map