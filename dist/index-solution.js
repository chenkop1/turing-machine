"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const ramda_1 = require("ramda");
const consts_1 = require("./consts");
const types_1 = require("./types");
const utils_1 = require("./utils");
/**
 * @param {[category: string, longitude: string, latitude: string]}
 * @returns {Target}
 */
const toTarget = ([category, longitude, latitude]) => ({ category, longitude: Number(longitude), latitude: Number(latitude) });
/**
 * @param {DecodeFunc} decodeFunc
 * @returns {(text: string) => Target[]}
 */
const extractTargets = decodeFunc => (0, ramda_1.pipe)(decodeFunc, (0, ramda_1.split)('\n'), (0, ramda_1.filter)((0, ramda_1.complement)(ramda_1.isEmpty)), (0, ramda_1.map)((0, ramda_1.split)(',')), (0, ramda_1.map)(toTarget));
/**
 * @param {DecodeFunc} decodeFunc
 * @returns {(text: string) => Target}
*/
const getMostWestern = decodeFunc => (0, ramda_1.pipe)(extractTargets(decodeFunc), (0, utils_1.reduce)((0, ramda_1.minBy)((0, ramda_1.prop)('longitude'))));
/**
 * @param {DecodeFunc} decodeFunc
 * @returns {(latitude: number) => (text: string) => TargetsAmountPartition}
*/
const countByLatitude = decodeFunc => latitude => (0, ramda_1.pipe)(
// first way
extractTargets(decodeFunc), (0, ramda_1.pluck)('latitude'), (0, ramda_1.partition)((0, ramda_1.gt)(ramda_1.__, latitude)), (0, ramda_1.map)(ramda_1.length), (0, ramda_1.zipObj)(['above', 'below'])
// second way
// countBy(
//     ifElse(
//         pipe(
//             prop('latitude'),
//             gt(__, latitude),
//         ),
//         always('above'),
//         always('below')
//     )
// )
);
/**
 * @type {(categories: Category[]) => Category}
*/
const getMostFrequent = (0, ramda_1.pipe)((0, ramda_1.countBy)(ramda_1.identity), Object.entries, (0, utils_1.reduce)((0, ramda_1.maxBy)((0, ramda_1.nth)(1))), ramda_1.head);
/**
 * @param {DecodeFunc} decodeFunc
 * @returns {(text: string) => Category}
*/
const getMostFrequentCategory = decodeFunc => (0, ramda_1.pipe)(extractTargets(decodeFunc), (0, ramda_1.pluck)('category'), getMostFrequent);
const targetsBase64Encoded = (0, fs_1.readFileSync)('./src/targets-base64.txt', { encoding: 'utf-8' });
const targetsSpecial17Encoded = (0, fs_1.readFileSync)('./src/targets-special-shifted-17.txt', { encoding: 'utf-8' });
const decodeSpecial17 = (0, utils_1.specialDecode)(17);
console.table({
    base64: {
        mostWestern: JSON.stringify(getMostWestern(atob)(targetsBase64Encoded)),
        countByLatitude: JSON.stringify(countByLatitude(atob)(consts_1.LATITUDE)(targetsBase64Encoded)),
        mostFrequent: getMostFrequentCategory(atob)(targetsBase64Encoded)
    },
    special17: {
        mostWestern: JSON.stringify(getMostWestern(decodeSpecial17)(targetsSpecial17Encoded)),
        countByLatitude: JSON.stringify(countByLatitude(decodeSpecial17)(consts_1.LATITUDE)(targetsSpecial17Encoded)),
        mostFrequent: getMostFrequentCategory(decodeSpecial17)(targetsSpecial17Encoded)
    }
});
// const targetsBase64 = extractTargets(atob)(targetsBase64Encoded);
// console.log(JSON.stringify(toGeoJson(targetsBase64)));
// const targetsSpecial17 = extractTargets(decodeSpecial17)(targetsSpecial17Encoded);
// console.log(JSON.stringify(toGeoJson(targetsSpecial17)));
//# sourceMappingURL=index-solution.js.map