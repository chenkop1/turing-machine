"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toGeoJson = exports.specialDecode = exports.reduce = void 0;
const ramda_1 = require("ramda");
const consts_1 = require("./consts");
const reduce = (reduceFunc) => (arr) => arr.reduce(reduceFunc);
exports.reduce = reduce;
const shiftChar = (shift) => (char) => String.fromCharCode(char.charCodeAt(0) + shift);
const shiftText = (shift) => (0, ramda_1.pipe)((0, ramda_1.split)(''), (0, ramda_1.map)(shiftChar(shift)), (0, ramda_1.join)(''));
const specialEncode = shiftText;
const specialDecode = (shift) => shiftText(-shift);
exports.specialDecode = specialDecode;
//#endregion special encoding
//#region geojson
const getProperties = (category) => ({
    category,
    "marker-size": 'medium',
    "marker-symbol": consts_1.CATEGORY_TO_PROPERTIES[category].symbol,
    "marker-color": consts_1.CATEGORY_TO_PROPERTIES[category].color
});
const toGeoJsonFeature = ({ category, longitude, latitude }, id) => ({
    type: 'Feature',
    id,
    geometry: {
        coordinates: [longitude, latitude],
        type: 'Point'
    },
    properties: getProperties(category)
});
const toGeoJson = (targets) => ({
    type: 'FeatureCollection',
    features: [
        ...targets.map(toGeoJsonFeature),
        consts_1.LATITUDE_LINE
    ]
});
exports.toGeoJson = toGeoJson;
//#endregion geojson
//# sourceMappingURL=utils.js.map