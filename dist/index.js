"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const consts_1 = require("./consts");
const utils_1 = require("./utils");
//#region implementing
const getMostWestern = ;
const countByLatitude = ;
const getMostFrequentCategory = ;
//#endregion implementing
//#region checking
/**
 * @type {string}
 */
const targetsBase64Encoded = ; // read the string from the file 'targets-base64.txt'
/**
 * @type {string}
 */
const targetsSpecial17Encoded = ; // read the string from the file 'targets-special-shifted-17.txt'
console.table({
    base64: {
        mostWestern: JSON.stringify(getMostWestern( /* using targetsBase64Encoded */)),
        countByLatitude: JSON.stringify(countByLatitude( /* using targetsBase64Encoded */)),
        mostFrequent: getMostFrequentCategory( /* using targetsBase64Encoded */)
    },
    special17: {
        mostWestern: JSON.stringify(getMostWestern( /* using targetsSpecial17Encoded */)),
        countByLatitude: JSON.stringify(countByLatitude( /* using targetsSpecial17Encoded */)),
        mostFrequent: getMostFrequentCategory( /* using targetsSpecial17Encoded */)
    }
});
//#endregion checking
//# sourceMappingURL=index.js.map