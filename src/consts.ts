import { GeoJsonFeature } from './types';

export const LATITUDE = 31.43774097567021;

export const LATITUDE_LINE: GeoJsonFeature = {
    type: 'Feature',
    properties: {},
    geometry: {
        coordinates: [
            [
                34.34252306591986,
                LATITUDE
            ],
            [
                34.45003171797467,
                LATITUDE
            ]
        ],
        type: 'LineString'
    }
};

export const CATEGORY_TO_PROPERTIES: Record<string, { symbol: string, color: string; }> = {
    hospital: { symbol: 'hospital', color: '#bd0000' },
    school: { symbol: 'college', color: '#8500cc' },
    mosque: { symbol: 'religious-muslim', color: '#000000' },
    government: { symbol: 'castle', color: '#008f00' },
    arsenal: { symbol: 'fire-station', color: '#cc7700' },
    'rockets-site': { symbol: 'rocket', color: '#0054ad' },
};