import { join, map, pipe, split } from "ramda";
import { CATEGORY_TO_PROPERTIES, LATITUDE_LINE } from "./consts";
import { Category, GeoJson, GeoJsonFeature, GeoJsonProperties, Target } from "./types";

export const reduce = <T>(reduceFunc: (acc: T, val: T) => T) =>
    (arr: T[]): T =>
        arr.reduce(reduceFunc);


//#region special encoding
/**
 * @description one-char string
*/
type Char = string;

const shiftChar = (shift: number) => (char: Char): Char => String.fromCharCode(char.charCodeAt(0) + shift);

const shiftText = (shift: number) =>
    pipe(
        split(''),
        map(shiftChar(shift)),
        join('')
    );

const specialEncode = shiftText;

export const specialDecode = (shift: number) => shiftText(-shift);
//#endregion special encoding


//#region geojson
const getProperties = (category: Category): GeoJsonProperties => ({
    category,
    "marker-size": 'medium',
    "marker-symbol": CATEGORY_TO_PROPERTIES[category].symbol,
    "marker-color": CATEGORY_TO_PROPERTIES[category].color
});

const toGeoJsonFeature = ({ category, longitude, latitude }: Target, id: number): GeoJsonFeature => ({
    type: 'Feature',
    id,
    geometry: {
        coordinates: [longitude, latitude],
        type: 'Point'
    },
    properties: getProperties(category)
});

export const toGeoJson = (targets: Target[]): GeoJson => ({
    type: 'FeatureCollection',
    features: [
        ...targets.map(toGeoJsonFeature),
        LATITUDE_LINE
    ]
});
//#endregion geojson