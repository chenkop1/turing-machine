import { readFileSync } from "fs";
import { __, complement, countBy, filter, gt, head, identity, isEmpty, length, map, maxBy, minBy, nth, partition, pipe, pluck, prop, split, zipObj } from "ramda";
import { LATITUDE } from "./consts";
import { DecodeFunc, Target, Category, TargetsAmountPartition } from "./types";
import { reduce, specialDecode, toGeoJson } from "./utils";

/**
 * @param {[category: string, longitude: string, latitude: string]} 
 * @returns {Target}
 */
const toTarget = ([category, longitude, latitude]) => ({ category, longitude: Number(longitude), latitude: Number(latitude) });

/**
 * @param {DecodeFunc} decodeFunc 
 * @returns {(text: string) => Target[]}
 */
const extractTargets = decodeFunc =>
    pipe(
        decodeFunc,
        split('\n'),
        filter(complement(isEmpty)),
        map(split(',')),
        map(toTarget)
    );


/**
 * @param {DecodeFunc} decodeFunc 
 * @returns {(text: string) => Target}
*/
const getMostWestern = decodeFunc =>
    pipe(
        extractTargets(decodeFunc),
        reduce(minBy(prop('longitude')))
    );


/**
 * @param {DecodeFunc} decodeFunc 
 * @returns {(latitude: number) => (text: string) => TargetsAmountPartition}
*/
const countByLatitude = decodeFunc =>
    latitude =>
        pipe(
            // first way
            extractTargets(decodeFunc),
            pluck('latitude'),
            partition(gt(__, latitude)),
            map(length),
            zipObj(['above', 'below'])

            // second way
            // countBy(
            //     ifElse(
            //         pipe(
            //             prop('latitude'),
            //             gt(__, latitude),
            //         ),
            //         always('above'),
            //         always('below')
            //     )
            // )
        );


/**
 * @type {(categories: Category[]) => Category}
*/
const getMostFrequent = pipe(
    countBy(identity),
    Object.entries,
    reduce(maxBy(nth(1))),
    head
);

/**
 * @param {DecodeFunc} decodeFunc 
 * @returns {(text: string) => Category}
*/
const getMostFrequentCategory = decodeFunc =>
    pipe(
        extractTargets(decodeFunc),
        pluck('category'),
        getMostFrequent
    );


const targetsBase64Encoded = readFileSync('./src/targets-base64.txt', { encoding: 'utf-8' });
const targetsSpecial17Encoded = readFileSync('./src/targets-special-shifted-17.txt', { encoding: 'utf-8' });
const decodeSpecial17 = specialDecode(17);

console.table({
    base64: {
        mostWestern: JSON.stringify(getMostWestern(atob)(targetsBase64Encoded)),
        countByLatitude: JSON.stringify(countByLatitude(atob)(LATITUDE)(targetsBase64Encoded)),
        mostFrequent: getMostFrequentCategory(atob)(targetsBase64Encoded)
    },
    special17: {
        mostWestern: JSON.stringify(getMostWestern(decodeSpecial17)(targetsSpecial17Encoded)),
        countByLatitude: JSON.stringify(countByLatitude(decodeSpecial17)(LATITUDE)(targetsSpecial17Encoded)),
        mostFrequent: getMostFrequentCategory(decodeSpecial17)(targetsSpecial17Encoded)
    }
});

// const targetsBase64 = extractTargets(atob)(targetsBase64Encoded);
// console.log(JSON.stringify(toGeoJson(targetsBase64)));
// const targetsSpecial17 = extractTargets(decodeSpecial17)(targetsSpecial17Encoded);
// console.log(JSON.stringify(toGeoJson(targetsSpecial17)));