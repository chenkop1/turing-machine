no copy-pasting or duplicate code. if code is duplicated a function should be extracted.

1. given a string containing numbers seperated by lines (maybe several lines)
return the sum of all the numbers
the solution should be a pipe

2. now suppose the numbers are encoded in special-encoding
the solution should just add a single stage to the pipeline of a `decode` function

now suppose that the encoding is with a different parameter
the solution should modify the `decode` function to receive said parameter
the solution should support any value of the parameter

now suppose that the text is encoded in a different encoding completely (not an adaptation of a parameter)
the solution should now receive the `decode` function as a parameter

now we need to get the sum of just the top 3 values in the input
the solution should just add a sort asc, take 3, sum BUT the last stages of the pipe (sum vs (sort asc, take 3, sum)) should 

now we need to get the multiplication of the lowest and largest number 
